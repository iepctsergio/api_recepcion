var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var UserSchema = Schema({
    nombre: String,
    email: String,
    password: String,
    fecha_creacion: Date,
    role: String,        
    activo: Boolean
});

module.exports = mongoose.model('User', UserSchema);