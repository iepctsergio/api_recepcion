'use strict'
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var APPUserSchema = Schema({
    IdUsuario:String,
    Nombre: String,    
    Password: String,
    Fecha_Creacion: Date,
    Rol: String,        
    Activo: Boolean,
    Identificador_SIEE:String,
    Estatus:String,
    Sincronizar:Boolean
});

module.exports = mongoose.model('APPUser', APPUserSchema);