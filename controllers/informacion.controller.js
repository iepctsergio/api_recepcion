'use strict'
let fs = require('fs');
let Global = require('../global');
var request = require('request');


const canal = 'nuevapublicacion';

function aregarInformacion(req, res) {
   
    let params = req;
    let files = params.files;
    let nombreOriginal = files.archivo.originalFilename;
    let patt = files.archivo.path;
    let file_name = 'sin imagen';
    let hashfile = '';

    console.log(patt);
    if (patt) {
        let file_path = patt;
        let file_split = file_path.split('/');
        file_name = file_split[1];
        let ext_split = file_name.split('.');
        let file_ext = ext_split[1];

        if (file_ext == 'json' || file_ext == 'Json' || file_ext == 'JSON') {
            console.log("--------------------------------------------------------------------");
            console.log("---- Archivo de Información: " + nombreOriginal);
            console.log("--------------------------------------------------------------------");
            console.log("path del archivo: "+file_path);

            fs.readFile(file_path, 'utf8', function (err, data) {
                if (err) {
                    console.log(err);
                }

                let datos = JSON.stringify(JSON.parse(data)); //usar esto para mandarlo a REDIS!!!

                console.log(Global.global.urlInformation);
                
                switch (nombreOriginal) {
                    // Descarga
                    
                    case 'descarga.json':
                        request.post(Global.global.urlInformation, 
                            { form: { data: datos, dataName: nombreOriginal } });
                        break;

                    // Diputaciones
                    case 'DIP_DET_ENTIDAD.json':
                        request.post(Global.global.urlInformation, 
                            { form: { data: datos, dataName: nombreOriginal } });
                        break;
                    case 'DIP_DISTRITO.json':
                    request.post(Global.global.urlInformation, 
                        { form: { data: datos, dataName: nombreOriginal } });                                       
                        break;
                    case 'DIP_ENTIDAD.json':
                    request.post(Global.global.urlInformation, 
                        { form: { data: datos, dataName: nombreOriginal } });                               
                        break;
                    case 'DIP_SECCION.json':
                    request.post(Global.global.urlInformation, 
                        { form: { data: datos, dataName: nombreOriginal } });                               
                        break;

                    //Gubernatura
                    case 'GOB_DET_ENTIDAD.json':
                    request.post(Global.global.urlInformation, 
                        { form: { data: datos, dataName: nombreOriginal } });                               
                        break;
                    case 'GOB_DISTRITO.json':
                    request.post(Global.global.urlInformation, 
                        { form: { data: datos, dataName: nombreOriginal } });                              
                        break;
                    case 'GOB_ENTIDAD.json':
                    request.post(Global.global.urlInformation, 
                        { form: { data: datos, dataName: nombreOriginal } });                               
                        break;
                    case 'GOB_SECCION.json':
                    request.post(Global.global.urlInformation, 
                        { form: { data: datos, dataName: nombreOriginal } });                               
                        break;

                    //Municipio
                    case 'MUN_DETALLE.json':
                    request.post(Global.global.urlInformation, 
                        { form: { data: datos, dataName: nombreOriginal } });                               
                        break;
                    case 'MUN_ENTIDAD.json':
                    request.post(Global.global.urlInformation, 
                        { form: { data: datos, dataName: nombreOriginal } });                               
                        break;
                    case 'MUN_SECCION.json':
                    request.post(Global.global.urlInformation, 
                        { form: { data: datos, dataName: nombreOriginal } });                               
                        break;
                    case 'MUNICIPIOS.json':
                    request.post(Global.global.urlInformation, 
                        { form: { data: datos, dataName: nombreOriginal } });                               
                        break;
                    //default
                    default:
                        break;
                }
            });
        };

        res.status(200).send({
            message: 'Archivo Recibido',
            nombreArchivo: nombreOriginal
        });
    }
    else {
        console.log("--------------------------------------------------------------------");
        console.log("--- Archivo Rechazado!!!... ---");
        console.log("--------------------------------------------------------------------\n\n");
        res.status(400).send({ message: 'Archivo Rechazado', nombreArchivo: nombreOriginal });
    }
}

module.exports = {
    aregarInformacion
}