'use strict'
function agregarActa(req, res) { 
    console.log(req);
    let params= req;    
    let files=params.files;    
    let nombreArchivoOrigen=files.imagen.originalFilename;       
    let patt=files.imagen.path;
    let DatosActa=params.DatosActa;
    let file_name = 'sin imagen';    
    if (patt) {
        let file_path = patt;            
        let file_split = file_path.split('\\');        
        file_name = file_split[1];
        let ext_split = file_name.split('.');
        let file_ext = ext_split[1];        
        if (file_ext == 'png'|| file_ext == 'PNG' || file_ext == 'jpg' || file_ext == 'JPG'){           

            //dar de alta el acta capturada y notificar a la api apuntando al siee que llego un acta y que a la vez notifique al cliente siee de que obtenga el acta recibida

            res.status(200).send({message:'Archivo Recibido', nombreArchivo:file_name,ArchivoOrigen:nombreArchivoOrigen});
        }
        else{
            //eliminar el archivo que llego
            res.status(400).send({message:'Archivo Rechazado', nombreArchivo:file_name, ArchivoOrigen:nombreArchivoOrigen});
        }
    }    
}
function identificarActa(req,res) {
    
}

module.exports = {
    agregarActa
};