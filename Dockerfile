FROM node:carbon
RUN apt-get install git-core 
WORKDIR /usr/src/app
RUN git clone https://iepctsergio@bitbucket.org/iepctsergio/api_recepcion.git
WORKDIR /usr/src/app/api_recepcion
RUN mkdir uploads
RUN npm install
CMD [ "npm", "start" ]